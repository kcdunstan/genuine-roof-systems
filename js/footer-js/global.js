/* Javascript that is not component specific but needs run globaly. */
'use strict';

(function(/* document */) {

  window.Grs = window.Grs || {};
  window.Grs.Utils = window.Grs.Utils || {};
  window.Grs.Helpers = window.Grs.Helpers || {};

  window.Grs.Utils.isMobile = function() {

    return window.getComputedStyle(document.querySelector('.nav-mobile-trigger'), null).getPropertyValue('display') === 'block';

  };

  window.Grs.Utils.requestFullScreen = function(element) {

    if (element.requestFullscreen) {

      element.requestFullscreen();

    } else if (element.mozRequestFullScreen) {

      element.mozRequestFullScreen();

    } else if (element.webkitRequestFullscreen) {

      element.webkitRequestFullscreen();

    } else if (element.msRequestFullscreen) {

      element.msRequestFullscreen();

    }

  };

  window.Grs.Utils.requestFullScreen = function(element) {

    if (element.requestFullscreen) {

      element.requestFullscreen();

    } else if (element.mozRequestFullScreen) {

      element.mozRequestFullScreen();

    } else if (element.webkitRequestFullscreen) {

      element.webkitRequestFullscreen();

    } else if (element.msRequestFullscreen) {

      element.msRequestFullscreen();

    }

  };


})(document, null);
