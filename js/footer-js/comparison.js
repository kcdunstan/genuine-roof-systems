'use strict';

(function(window) {

  window.Grs = window.Grs || {};
  window.Grs.Utils = window.Grs.Utils || {};
  window.Grs.Helpers = window.Grs.Helpers || {};

  if (document.querySelectorAll('.comparison-demo-link').length) {

    window.Grs.Comparison = (function() {

      const comparisonTriggers = document.querySelectorAll('.comparison-demo-link'),
            visibleMobileComparisonClass = 'comparison-video-visible',
            comparisonMask = document.querySelector('.comparison-video-container'),
            comparisonVideoPlayer = comparisonMask.querySelector('video'),
            comparisonCloseButton = comparisonMask.querySelector('.comparison-video-close');

      let isComparisonVideoVisible = false;

      for (let i = 0; i < comparisonTriggers.length; i++) {
        let comparisonTrigger = comparisonTriggers[i];

        comparisonTrigger.addEventListener('click', function(e) {

          //e.preventDefault();
          comparisonVideoPlayer.setAttribute('src', e.target.getAttribute('data-video-url'));
          comparisonTrigger.showComparison();

        });

        comparisonCloseButton.addEventListener('click', function() {

          comparisonTrigger.hideComparison();

        });

        comparisonTrigger.resetVideoSource = function() {

          comparisonVideoPlayer.pause();
          comparisonVideoPlayer.currentTime = 0;

        };

        comparisonTrigger.toggleComparison = function() {

          if (isComparisonVideoVisible) {

            comparisonTrigger.hideComparison();

          } else {

            comparisonTrigger.showComparison();

          }

          isComparisonVideoVisible = !isComparisonVideoVisible;

        };

        comparisonTrigger.hideComparison = function() {

          comparisonMask.classList.remove(visibleMobileComparisonClass);
          comparisonTrigger.resetVideoSource();

        };

        comparisonTrigger.showComparison = function() {

          comparisonMask.classList.add(visibleMobileComparisonClass);
          comparisonVideoPlayer.play();
          if (window.Grs.Utils.isMobile()) {
            window.Grs.Utils.requestFullScreen(comparisonVideoPlayer);
          }
          comparisonVideoPlayer.onended = function() {
            comparisonTrigger.hideComparison();
          };

        };
      }

    })(document, null);

  }

})(window);
