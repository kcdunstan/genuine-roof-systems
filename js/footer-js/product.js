'use strict';

(function(window) {

  window.Grs = window.Grs || {};
  window.Grs.Utils = window.Grs.Utils || {};
  window.Grs.Helpers = window.Grs.Helpers || {};




  window.Grs.Product = (function() {

    let carouselInner,
        thumbnails,
        thumbnailLinks;

    //let self;

    return {
      init: function() {

        carouselInner = document.querySelector('.product-carousel-inner');
        thumbnails = document.querySelector('.product-carousel-thumbnails');
        thumbnailLinks = thumbnails.querySelectorAll('.product-carousel-thumbnail-link');
        //self = this;

        for (let i = 0; i < thumbnailLinks.length; i++) {
          let thumbnailLink = thumbnailLinks[i];

          thumbnailLink.addEventListener('click', function(e) {

            let updatedLeftPosition = -Number(e.target.getAttribute('data-slide-number')) * 100 + '%';

            e.preventDefault();

            if (thumbnails.querySelector('.selected')) {
              thumbnails.querySelector('.selected').classList.remove('selected');
            }

            e.target.classList.add('selected');
            carouselInner.style.left = updatedLeftPosition;
            //carousel.selectCell(Number(e.target.getAttribute('data-slide-number')) - 1);

          });

        }

        thumbnailLinks[0].classList.add('selected');

      }

    };

  })(document, null);

})(window);

document.addEventListener('DOMContentLoaded', function() {

  if (document.querySelector('.product-carousel') !== null) {

    window.Grs.Product.init();

  }

});
