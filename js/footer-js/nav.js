'use strict';

(function(window) {

  window.Grs = window.Grs || {};
  window.Grs.Utils = window.Grs.Utils || {};
  window.Grs.Helpers = window.Grs.Helpers || {};

  window.Grs.Navigation = (function() {

    const navMobileTrigger = document.querySelector('.nav-mobile-trigger'),
          navMask = document.querySelector('.nav-mask'),
          visibleMobileNavClass = 'mobile-nav-visible',
          navItemsWithChildren = document.querySelectorAll('.menu-mobile-nav .menu-item-has-children'),
          navItemsWithChildrenOpenClass = 'open';

    let self,
        isMobileNavVisible = false;

    return {
      init: function() {

        self = this;

        navMobileTrigger.addEventListener('click', function(e) {

          e.preventDefault();
          self.toggleNav();

        });

        navMask.addEventListener('click', function() {

          self.toggleNav();

        });

        for (let i = 0; i < navItemsWithChildren.length; i++) {

          let item = navItemsWithChildren[i];

          item.querySelector('a').addEventListener('click', function(e) {

            e.preventDefault();
            this.parentNode.classList.toggle(navItemsWithChildrenOpenClass);

          });

        }
      },

      toggleNav: function() {

        if (isMobileNavVisible) {

          self.hideNav();

        } else {

          self.showNav();

        }

        isMobileNavVisible = !isMobileNavVisible;

      },

      hideNav: function() {

        document.body.classList.remove(visibleMobileNavClass);

      },

      showNav: function() {

        document.body.classList.add(visibleMobileNavClass);

      }

    };

  })(document, null);

})(window);

document.addEventListener('DOMContentLoaded', function() {

  window.Grs.Navigation.init();

});
