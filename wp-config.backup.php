<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'guthries_db');

/** MySQL database username */
define('DB_USER', 'guthries_db');

/** MySQL database password */
define('DB_PASSWORD', 'fireal');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '<W!Ez~rABQBz%:Yoh;Xsr4M,!-<`#d(Ryc<5Gal.mEfvX$qX^I}@$yGCi39eW&A`');
define('SECURE_AUTH_KEY',  'a;#-0hosJ%+EZ,qQzS.}~doT#Von7H!g.3R}4dV7PFr^A!j9EEPumxAPz2/^LffG');
define('LOGGED_IN_KEY',    '1,D{!<nP>[$Eh=Rlb*O}l8gb8xTs]Y&._^D=]Fl&Oi[dVHBnEFts5?e$._C%^Lzh');
define('NONCE_KEY',        'aU=~fEph+v,dFixYj90*w!p99qEX(d,_aJSe{PoaM}kRvOZ9o<h<)9D+F;iVt5Q1');
define('AUTH_SALT',        'E8XD5T,mA%xH8dywOn,*VG6w% )Vk8sBtUN`y@?NX<vGOQS]@d`u@q`U0~MYqYmL');
define('SECURE_AUTH_SALT', 'ov;*mq]t**_(sFqR;^EOIRMm1C=/Z%t2!OuRJTME}AF5b8k |yBx08Dn{Vh04-F ');
define('LOGGED_IN_SALT',   '5=lQjZcis)o!3JTP%7na.?6*0V~Z?.~/r,fYa>?TW]}zhe|tRM=Jn)!9{OrA_u}R');
define('NONCE_SALT',       '0N:OC XkE.RSG *L4tiU<1-1?/s.>7JG]6yHkFBO@JKdutK7D;zWaz:VjHz2lE5t');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
