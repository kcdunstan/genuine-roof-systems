<?php
/**
* Template Name: Product Overview
*
*/
?>

<?php get_header(); ?>


  <div class="main">
		<?php while ( have_posts() ) : the_post(); ?>

		  <?php
  		  // check if the repeater field has rows of data
  		  // TO DO: build a carousel nav while looping through the rows
  		  /// TO DO: build HTML for section at the bottom of the page with descriptions and images from the carousel data


        if (have_rows('carousel') ):
          $product_logo = get_field('product_logo');
          $carousel_nav_markup = [];
          $finish_overview_markup = [];
          $is_display_finish_overview_markup = false;
          $mobile_slides_markup = [];
          $current_slide_count = 0;

          function generateCarouselNavItem ($current_slide_count, $image, $title) {
            $markup = '<li class="product-carousel-thumbnail">';
            $markup .= '<button data-slide-number="' . $current_slide_count . '" class="product-carousel-thumbnail-link" style="background-image: url(\'' . $image['url'] . '\'">';
            $markup .= '<span class="sr-only">' . $title . '</span>';
            $markup .= '</button>';
            $markup .= '</li>';

            return $markup;
          };

          function generateFinishOverview ($current_slide_count, $image, $title, $description) {
            $markup = '<div id="product-finish-overview-' . $current_slide_count . '" class="product-finish-overview col-6-12-desktop">';
            $markup .= '<img class="product-finish-overview-image" src="'. $image['url'] .'" alt="'. $image['alt'] .'" />';
            $markup .= '<div class="product-finish-overview-body"><p><strong>'. $title;
            if($description) {
              $markup .= ':';
            }
            $markup .= '</strong></p>' . $description . '</div>';
            $markup .= '</div>';

            return $markup;
          };

          function generateMobileSlide ($orange_text, $headline, $product_image, $product_logo, $current_slide_count, $display_finish_overview_description) {
            $markup = '<div class="carousel-cell carousel-cell-mobile">';
            $markup .= '<div class="grid-mobile">';
            $markup .= '<div class="col-6-12-mobile">';
            $markup .= '<h2 class="carousel-header"><span class="carousel-subheader">' . $orange_text . '</span> ' .  $headline . '</h2>';
            $markup .= '<img class="carousel-product-image-mobile" alt="' . $product_logo['alt'] . '" src="' . $product_logo['url'] .'" />';
            $markup .= '</div>';
            $markup .= '<div class="col-6-12-mobile">';
            $markup .= '<img alt="' . $product_image['alt'] . '" src="' . $product_image['url'] . '" />';

            if($display_finish_overview_description) {
              $markup .= '<a class="btn btn-learn-more" href="#product-finish-overview-' . $current_slide_count . '">';
              $markup .= 'Learn More';
              $markup .= '</a>';
            }

            $markup .= '</div>';
            $markup .= '</div>';
            $markup .= '</div>';

            return $markup;
          }
        ?>
        <div class="product-carousel-wrapper">
          <div class="product-carousel-background" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>');"></div>
          <div class="container product-carousel-container">
            <div class="grid-desktop grid-mobile">

              <div class="col-4-12-desktop">
                <img class="product-logo" alt="<?php echo $product_logo['alt']; ?>" src="<?php echo $product_logo['url']; ?>" />
              </div>

              <div class="col-8-12-desktop col-12-12-mobile">
                <div class="product-carousel"
                id="colors">
                  <div class="product-carousel-inner">
                  <?php // loop through the rows of data
                  while ( have_rows('carousel') ) : the_row();

                    $orange_text = get_sub_field('orange_text');
                    $headline = get_sub_field('headline');
                    $body = get_sub_field('body');
                    $product_image = get_sub_field('large_product_image');
                    $product_image_thumbnail = get_sub_field('thumbnail_product_image');
                    $call_to_action = get_sub_field('call_to_action');

                    array_push($carousel_nav_markup, generateCarouselNavItem($current_slide_count, $product_image_thumbnail, $headline));

                    array_push($finish_overview_markup, generateFinishOverview($current_slide_count, $product_image_thumbnail, $headline, $body));

                    array_push($mobile_slides_markup, generateMobileSlide ($orange_text, $headline, $product_image, $product_logo, $current_slide_count, $body));

                    $is_display_finish_overview_markup = $body ? true : false;

                    $current_slide_count++;
                  ?>
                  <div class="carousel-cell product-carousel-cell">
                      <div class="grid-mobile">
                        <div class="col-6-12-mobile">
                          <h2 class="carousel-header">
                            <span class="carousel-subheader"><?php echo $orange_text; ?></span>
                            <?php echo $headline; ?>
                          </h2>
                        </div>

                        <div class="grid-desktop col-6-12-mobile">
                          <?php if ($body) : ?>
                          <div class="col-4-12-desktop">

                            <img alt="<?php echo $product_image['alt']; ?>" src="<?php echo $product_image['url']; ?>" />

                            <?php if ($call_to_action) : ?>
                            <span class="text-center">
                              <a href="<?php echo $call_to_action['url'] ?>"
                              class="btn btn-text btn-weathering"
                              target="<?php echo $call_to_action['target']; ?>">
                                <?php echo $call_to_action['title']; ?>
                              </a>
                            </span>
                            <?php endif; ?>

                            <a class="btn btn-learn-more"
                              href="#product-finish-overview-<?php echo $current_slide_count; ?>">
                              Learn More
                            </a>

                          </div>

                          <div class="col-8-12-desktop product-carousel-body">
                            <?php echo $body; ?>
                          </div>

                          <?php else : ?>
                            <div class="col-12-12-desktop product-carousel-image-only">
                              <img alt="<?php echo $product_image['alt']; ?>" src="<?php echo $product_image['url']; ?>" />
                            </div>
                          <?php endif; ?>

                        </div>
                      </div>

                    </div>


                    <?php
                    endwhile;
                    ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="product-carousel-thumbnails-container">
            <h4 class="product-carousel-thumbnails-header">Choose Your Color:</h4>
            <ul class="product-carousel-thumbnails">
              <?php
                echo implode('', $carousel_nav_markup);
              ?>
            </ul>
            <div class="clearfix"></div>
          </div>
        </div>

        <!-- mobile carousel -->
        <div class="carousel product-carousel-mobile"
          data-flickity='{ "cellSelector": ".carousel-cell-mobile",
            "pageDots": false,
            "wrapAround": true }'>
          <div class="product-carousel-background" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>');"></div>
          <?php
            echo implode('', $mobile_slides_markup);
          ?>
        </div>
        <!-- end mobile carousel -->

        <?php

        else :

            // no rows found

        endif;


		    ?>
		    <?php
  		  // check if the repeater field has rows of data
        if (have_rows('section_1') ):
          while( have_rows('section_1') ) : the_row(); ?>

          <div class="product-container">
            <div class="product-section-1">
              <div class="grid-desktop">
                <div class="col-7-12-desktop product-section-1-column-left">
                  <div class="page-header-container">
                    <?php
                      echo '<h2 class="page-subheader">' . get_sub_field('header') . '</h2>';
                    ?>
                  </div>
                  <?php the_sub_field('left_column'); ?>
                </div>

                <div class="col-5-12-desktop product-section-1-column-right">
                  <?php while( have_rows('right_column') ) : the_row(); ?>

                    <?php
                      echo wp_get_attachment_image( get_sub_field('image')['id'], 'full', false, array( 'class' => 'product-image lazyload', 'data-sizes' => 'auto' ) );
                      echo '<div class="product-caption-container">' . get_sub_field('caption') . '</div>';
                    ?>


                  <?php endwhile; ?>
                </div>
              </div>
            </div>
          </div>
        <?php
          endwhile;
        endif; ?>


        <?php
  		  // check if the repeater field has rows of data
        if (have_rows('section_2') ):
          while ( have_rows('section_2') ) : the_row();

          $section_2_overview_header = get_sub_field('header');
          $body = get_sub_field('right_column');

        ?>


        <div class="product-section-2-wrapper wrapper wrapper-grey">
          <div class="product-section-2-container container clearfix">
            <h2 class="text-left page-subheader product-section-2-header"><?php echo $section_2_overview_header; ?></h2>

            <?php // loop through the rows of data
            while ( have_rows('left_column') ) : the_row();

              $overview_image = get_sub_field('left_column_image');
              $overview_photo = get_sub_field('left_column_photo');
              $overview_image_header = get_sub_field('left_column_header');
              $overview_image_subheader = get_sub_field('left_column_subheader');
              $overview_call_to_action = get_sub_field('left_column_call_to_action');

          ?>

          <div class="grid-desktop">
            <div class="col-5-12-desktop product-section-2-column-left">

              <?php echo wp_get_attachment_image( $overview_image['id'], 'full', false, array( 'class' => 'lazyload product-image', 'data-sizes' => 'auto' ) ); ?>

              <h4><strong class="block"><?php echo $overview_image_header; ?></strong> <?php echo $overview_image_subheader; ?></h4>

              <?php

              if ($overview_call_to_action) {

                echo '<p class="text-center"><a href="'. $overview_call_to_action['url'] .'" target="' . $overview_call_to_action['target'] . '" class="btn btn-centered">' . $overview_call_to_action['title'] . '</a></p>';

              }

              echo wp_get_attachment_image( $overview_photo['id'], 'full', false, array( 'class' => 'lazyload product-image', 'data-sizes' => 'auto' ) );

              ?>

            </div>

            <div class="col-7-12-desktop product-section-2-column-right">
              <?php
                echo $body;
              ?>
            </div>
          </div>

          <?php
              endwhile;
            endwhile;
          ?>
          </div>
        </div>
          <?php else :

              // no rows found

          endif;

          // check if the repeater field has rows of data
          if (have_rows('section_3') ):
            while( have_rows('section_3') ) : the_row(); ?>

            <div class="product-section-3-container container clearfix">
              <div class="grid-desktop">
                <div class=" col-8-12-desktop product-section-3-column-left">
                  <?php the_sub_field('body'); ?>
                </div>

                <div class="col-4-12-desktop sidebar product-section-3-column-right">
                  <?php while( have_rows('sidebar') ) : the_row(); ?>
                    <?php while( have_rows('content') ) : the_row(); ?>
                      <?php
                        if( get_row_layout() == 'header' ):

                          $header_text = get_sub_field('header');
                          $header_background_color = 'bg-' . get_sub_field('header_background_color');
                      ?>
                          <h4 class="sidebar-header <?php echo $header_background_color ?>">
                            <?php echo $header_text; ?>
                          </h4>

                      <?php elseif( get_row_layout() == 'block_image' ):

                          $sidebar_seal = get_sub_field('block_image');
                          echo wp_get_attachment_image( $sidebar_seal['id'], 'full', false, array( 'class' => 'lazyload', 'data-sizes' => 'auto' ) );

                          elseif( get_row_layout() == 'body' ): ?>

                        <div class="sidebar-body clearfix">
                        <?php
                          the_sub_field('body');
                        ?>
                        </div>

                      <?php endif; ?>
                  <?php
                      endwhile;
                    endwhile; ?>
                </div>
              </div>
              <?php
                endwhile;
              endif; ?>
          </div>

          <?php
  		  // check if the repeater field has rows of data
        if (have_rows('section_4') ):
          while( have_rows('section_4') ) : the_row(); ?>
          <div class="wrapper-grey">
            <div class="product-container">
              <div class="product-section-4">
                <div class="page-header-container">
                  <?php
                    the_sub_field('header');
                  ?>
                </div>
                <div class="grid-desktop">

                  <div class="col-6-12-desktop product-section-4-column-left">
                    <?php the_sub_field('column_1'); ?>
                  </div>

                  <div class="col-6-12-desktop product-section-4-column-right">
                    <?php the_sub_field('column_2'); ?>
                  </div>

                </div>

                <hr />

                <div>
                  <div class="page-header-container">
                    <?php
                      echo '<h2 class="page-subheader text-center">' . get_sub_field('header_2') . '</h2>';
                    ?>
                  </div>
                  <?php
                    echo '<p class="text-center"><a href="'. get_sub_field('call_to_action')['url'] .'" target="' . get_sub_field('call_to_action')['target'] . '" class="btn btn-centered">' . get_sub_field('call_to_action')['title'] . '</a></p>';
                    echo '<p class="disclaimer text-center">' . get_sub_field('disclaimer') . '</p>';
                  ?>
                </div>
              </div>
            </div>
          </div>
        <?php
          endwhile;
        endif; ?>


  		  <?php
  		  // check if the repeater field has rows of data
        if (have_rows('section_5') ):
          while( have_rows('section_5') ) : the_row();
          $images = get_sub_field('gallery'); ?>

          <?php if( $images ): ?>
            <div id="gallery">
              <div class="product-container">
                <div class="product-section-5">
                  <div class="page-header-container">
                    <?php
                      echo '<h2 class="page-subheader">' . get_sub_field('header') . '</h2>';
                    ?>
                  </div>
                  <?php the_sub_field('intro'); ?>
                  <div class="product-gallery"
                    data-flickity='{"imagesLoaded": true,
                                    "pageDots": false,
                                    "prevNextButtons": false,
                                    "wrapAround": true }'>
                    <?php foreach( $images as $image ): ?>
                      <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    <?php endforeach; ?>
                  </div>
                  <div class="product-gallery-thumbnails"
                        data-flickity='{"asNavFor": ".product-gallery",
                                    		"cellAlign": "center",
                                    		"contain": true,
                                        "groupCells": true,
                                    		"imagesLoaded": true,
                                    		"pageDots": false,
                                    		"wrapAround": false
                                      }'>
                    <?php foreach( $images as $image ): ?>
                      <img class="product-gallery-thumbnail" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?> thumbnail" />
                    <?php endforeach; ?>
                  </div>
                  <p class="disclaimer text-right"><?php the_sub_field('disclaimer'); ?></p>
                </div>
              </div>
            </div>

        <?php endif;
          endwhile;
        endif;
        ?>

		  <?php
  		  // check if the repeater field has rows of data
        if (have_rows('section_6') ):
          while( have_rows('section_6') ) : the_row(); ?>
            <div id="testimonials" class="product-container">
              <div class="product-section-6">
                <div class="page-header-container">
                  <h2 class="text-left page-subheader">
                    <?php echo get_sub_field('header'); ?>
                  </h2>
                </div>

                <?php the_sub_field('intro');

                if (have_rows('testimonials') ): ?>

                  <div class="carousel-testimonials"
                    data-flickity='{"autoPlay": false,
                        "wrapAround": true }'>

                      <?php while ( have_rows('testimonials') ) : the_row(); ?>
                        <div class="carousel-cell carousel-testimonial">
                          <div class="carousel-testimonial-body">
                            <?php echo get_sub_field('body'); ?>
                          </div>
                          <div class="carousel-testimonial-byline">
                            <?php echo get_sub_field('byline'); ?>
                          </div>
                        </div>
                      <?php endwhile; ?>
                  </div>

                <?php endif; ?>
              </div>
            </div>
        <?php
          endwhile;
        endif; ?>

      <?php
  		  // check if the repeater field has rows of data
        if (get_field('section_7') ): ?>
            <div class="product-container">
              <div id="weathering-information" class="product-section-7">
                <?php the_field('section_7'); ?>
              </div>
            </div>
      <?php
        endif; ?>

		<?php endwhile;
  		if($is_display_finish_overview_markup) :
		?>

		  <div class="product-container">
        <div class="product-section-8">
        <p><strong><?php echo get_field('section_8_header'); ?></strong></p>

        <div class="product-finish-overviews grid-desktop">
      		<?php echo implode('', $finish_overview_markup); ?>
    		</div>
      </div>
    <?php endif; ?>
  </div>


<?php get_footer(); ?>
