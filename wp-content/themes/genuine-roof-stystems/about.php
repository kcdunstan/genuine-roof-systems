<?php
/**
* Template Name: About
*
*/
?>

<?php get_header(); ?>


  <div class="main">
		<?php while ( have_posts() ) : the_post(); ?>

        <?php if ( has_post_thumbnail() ) : ?>
          <div class="hero">
            <?php the_post_thumbnail(); ?>
          </div>
        <?php endif; ?>


        <div class="about-container">
          <div class="page-header-container">
            <?php
              the_title('<h1 class="page-header">', '</h1>');
              echo '<h2 class="page-subheader">' . get_field('subheader') . '</h2>';
            ?>
          </div>
          <div class="grid-desktop">
            <div class="about-image-1">
              <?php
                $about_image_1 = get_field('image_1');
                $about_image_2 = get_field('image_2');
                echo '<img class="about-image" src="' . $about_image_1['url'] . '" alt="' . $about_image_1['alt'] . '" />';
              ?>
              <picture class="about-image-2-desktop">
                <source srcset="<?php echo $about_image_2['url']; ?>" media="(min-width: 920px)">
                <img src=""
                  class="about-image"
                  alt="<?php echo $about_image_2['alt']; ?>" />
              </picture>
            </div>
            <div class="about-text-block-1">
              <?php the_field('text_block_1'); ?>
              <picture class="about-image-2">
                <source srcset="<?php echo $about_image_2['url']; ?>" media="(max-width: 919px)">
                <img src=""
                  class="about-image"
                  alt="<?php echo $about_image_2['alt']; ?>" />
              </picture>
              <?php the_field('text_block_2'); ?>
            </div>
          </div>
        </div>

        <div class="about-form about-form-wrapper" id="free-consultation">
          <div class="container about-form-container">
            <?php echo do_shortcode("[vfb id='2']"); ?>
          </div>
        </div>


		<?php endwhile; ?>
    </div>
  </div>

<?php get_footer(); ?>
