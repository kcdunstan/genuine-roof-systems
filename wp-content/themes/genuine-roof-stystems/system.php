<?php
/**
* Template Name: System
*
*/
?>

<?php get_header(); ?>


  <div class="main">
		<?php while ( have_posts() ) : the_post(); ?>

        <?php if ( has_post_thumbnail() ) : ?>
          <div class="hero">
            <?php the_post_thumbnail('full', array('class' => 'hero-image')); ?>
          </div>
        <?php endif; ?>


        <div class="system-container">
          <div class="page-header-container">
            <?php
              the_title('<h1 class="page-header">', '</h1>');
              echo '<h2 class="page-subheader">' . get_field('subheader') . '</h2>';
            ?>
          </div>

          <?php
      		  // check if the repeater field has rows of data
            if (have_rows('section_1') ):
              while( have_rows('section_1') ) : the_row(); ?>
              <div class="system-section-1">
                <div class="grid-desktop">
                  <div class=" col-7-12-desktop system-section-1-column-left">
                    <?php the_sub_field('left_column'); ?>
                  </div>

                  <div class="col-5-12-desktop sidebar system-section-1-column-right">
                    <?php while( have_rows('right_column') ) : the_row(); ?>

                      <?php
                        if( get_row_layout() == 'header' ):

                          $header_text = get_sub_field('header');
                          $header_background_color = 'bg-' . get_sub_field('header_background_color');
                      ?>
                          <h4 class="sidebar-header <?php echo $header_background_color ?>">
                            <?php echo $header_text; ?>
                          </h4>

                      <?php elseif( get_row_layout() == 'body' ): ?>

                        <div class="sidebar-body clearfix">
                        <?php
                          $sidebar_seal = get_sub_field('image');
                          echo '<img class="sidebar-seal" src="' . $sidebar_seal['url'] . '" alt="' . $sidebar_seal['alt'] . '" />';

                          the_sub_field('body');
                        ?>
                        </div>

                      <?php endif; ?>


                    <?php endwhile; ?>
                  </div>
                </div>
              </div>
            <?php
              endwhile;
            endif; ?>
        </div>

        <?php
        $section_2 = get_field('section_2');

        if ($section_2) : ?>

        <div class="system-section-2 bg-blue">
          <div class="system-container text-color-white">
            <?php
              echo $section_2['body'];
            ?>
            <a href="<?php echo $section_2['call_to_action']['url']; ?>" class="btn btn-orange" title="<?php echo $section_2['call_to_action']['title']; ?>">
              <?php echo $section_2['call_to_action']['title']; ?>
            </a>
          </div>
        </div>

        <?php endif; ?>

         <?php
      		  // check if the repeater field has rows of data
            if (have_rows('section_3') ):
              while( have_rows('section_3') ) : the_row(); ?>
              <div class="system-container">
                <div class="system-section-3">
                  <h2 class="page-subheader"><?php echo the_sub_field('header'); ?></h2>
                  <?php
            		  // check if the repeater field has rows of data
                  if (have_rows('resources') ): ?>
                    <ul class="grid-desktop system-resources">
                    <?php while( have_rows('resources') ) : the_row(); ?>
                      <li class="col-4-12-desktop system-resource">
                        <?php
                          $image = get_sub_field('image');
                          echo '<img class="system-resources-icon" src="' . $image['url'] . '" alt="' . $image['alt'] . '" />'; ?>
                          <h3 class="system-resources-header">
                            <?php the_sub_field('resource_header'); ?>
                          </h3>

                          <?php if (have_rows('calls_to_action') ): ?>
                            <ul class="system-resources-ctas">
                            <?php while( have_rows('calls_to_action') ) : the_row();
                              $call_to_action = get_sub_field('call_to_action');
                              $call_to_action_background_color = 'btn-' . get_sub_field('call_to_action_color');
                            ?>
                              <li class="system-resources-cta">
                                <a href="<?php echo $call_to_action['url']; ?>" class="btn <?php echo $call_to_action_background_color ?>" href="<?php echo $call_to_action['title']; ?>">
                                  <?php echo $call_to_action['title']; ?>
                                </a>
                              </li>
                            <?php endwhile; ?>
                            </ul>
                          <?php endif; ?>
                      </li>
                    <?php endwhile; ?>
                    </ul>
                  <?php endif; ?>


                </div>
              </div>
            <?php
              endwhile;
            endif; ?>

        </div>

		<?php endwhile; ?>
    </div>
  </div>


<?php get_footer(); ?>
