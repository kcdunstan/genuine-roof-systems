<?php
/**
* Template Name: Installation
*
*/
?>

<?php get_header(); ?>


  <div class="main">
		<?php while ( have_posts() ) : the_post(); ?>

        <?php if ( has_post_thumbnail() ) : ?>
          <div class="hero">
            <?php the_post_thumbnail(); ?>
          </div>
        <?php endif; ?>


        <div class="installation-container installation-intro">
          <div class="page-header-container">
            <?php
              the_title('<h1 class="page-header">', '</h1>');
              echo '<h2 class="page-subheader">' . get_field('subheader') . '</h2>';
            ?>
          </div>
          <?php
          $section_1 = get_field('section_1');

          if ($section_1) : ?>

          <div class="grid-desktop">
            <div class="installation-text-block-1">
              <?php echo $section_1['text']; ?>
            </div>
            <img src="<?php echo $section_1['image']['url']; ?>"
                class="installation-image installation-image-1" alt="<?php echo $$section_1['image']['alt']; ?>" />
          </div>

          <?php endif; ?>

        </div>

        <?php
  		  // check if the field has data

        if (get_field('section_2') ):

          $video_header = get_field('header');
          $video_file = get_field('video_file');
          $video_embedded_youtube_url = get_field('video_embedded_youtube_url');
          $video_caption = get_field('video_caption');
          $video_aspect_ratio = get_field('video_aspect_ratio');

        ?>
        <?php while( have_rows('section_2') ) : the_row(); ?>
        <div class="wrapper wrapper-grey">
          <div class="video-container installation-container container">
            <h2 class="text-left page-subheader"><?php echo get_sub_field('header'); ?></h2>
            <?php the_sub_field('body'); ?>

            <?php if ($video_caption): ?>

              <?php echo $video_caption; ?>

            <?php endif; ?>

            <?php if (get_sub_field('embedded_playlist_url') ): ?>

            <div class="embed-responsive embed-responsive-<?php echo get_sub_field('video_aspect_ratio'); ?>">
              <iframe class="embed-responsive-item" src="<?php the_sub_field('embedded_playlist_url'); ?>" allowfullscreen></iframe>
            </div>

            <?php endif; ?>
          </div>

        </div>

          <?php endwhile;
          endif; ?>

        <?php
          $section_3 = get_field('section_3');

          if ($section_3) : ?>
          <div class="wrapper">
            <div class="container installation-container installation-section-3">
              <h2 class="text-left page-subheader"><?php echo $section_3['header']; ?> <span class="subheader"><?php echo $section_3['subheader']; ?></span></h2>

              <div class="grid-desktop">
                <div class="installation-section-3-tools">
                  <?php echo $section_3['tools']; ?>
                </div>
                <img src="<?php echo $section_3['photo']['url']; ?>"
                    class="installation-image installation-section-3-photo" alt="<?php echo $$section_3['photo']['alt']; ?>" />
              </div>

              <div class="grid-desktop">
                <div class="installation-section-3-instructions">
                  <?php echo $section_3['instructions']; ?>
                </div>

                <div class="installation-section-3-disclaimer">
                  <?php echo $section_3['disclaimer']; ?>
                </div>
              </div>

              <img src="<?php echo $section_3['diagram']['url']; ?>"
                    class="installation-section-3-diagram" alt="<?php echo $section_3['diagram']['alt']; ?>" />

              <?php
        		  // check if the repeater field has rows of data
        		  $calls_to_action = $section_3['calls_to_action_2'];

              if ($calls_to_action): ?>
                <div class="installation-section-3-ctas">
                <?php foreach ($calls_to_action as $value) {
                  $call_to_action = $value['call_to_action'];
                  $call_to_action_background_color = 'btn-' . $value['call_to_action_color'];
                ?>
                    <a href="<?php echo $call_to_action['url']; ?>" class="btn <?php echo $call_to_action_background_color ?> installation-section-3-cta" href="<?php echo $call_to_action['title']; ?>">
                      <?php echo $call_to_action['title']; ?>
                    </a>
                <?php } ?>
                </div>
              <?php endif; ?>

              <hr class="container" />

            </div>
          </div>

          <?php endif; ?>

        <?php
          if (have_rows('section_4') ) : ?>
          <div class="container installation-container installation-section-4">
            <?php while( have_rows('section_4') ) : the_row(); ?>
            <h2 class="text-left page-subheader"><?php echo get_sub_field('header'); ?> <span class="subheader"><?php echo  get_sub_field('subheader'); ?></span></h2>

            <?php
              if (have_rows('steps') ):
                echo '<ol class="installation-steps grid-desktop">';
                while( have_rows('steps') ) : the_row();
            ?>
                <li class="installation-step col-6-12-desktop">
                  <h3 class="installation-step-header"><?php the_sub_field('header'); ?></h3>
                  <?php
                  if (get_sub_field('body')) : ?>

                    <div class="installation-step-body">
                      <?php the_sub_field('body'); ?>
                    </div>

                  <?php

                  endif;

                  if( get_sub_field('diagram') ) :

                    echo '<img class="installation-step-diagram" src="' . get_sub_field('diagram')['url'] . '" alt="' . get_sub_field('diagram')['alt'] . '" />';

                  endif;

                  ?>

                </li>
            <?php
                endwhile;
                echo '</ol>';
              endif;
            endwhile;
            ?>

            <hr class="container" />

          </div>

          <?php endif; ?>

        <?php
  		  // check if the repeater field has rows of data
        if (have_rows('section_5') ):
          while( have_rows('section_5') ) : the_row(); ?>
          <div class="installation-container">
            <div class="installation-section-5">
              <h2 class="page-subheader"><?php echo the_sub_field('header'); ?></h2>
              <?php echo the_sub_field('introduction'); ?>
              <?php
        		  // check if the repeater field has rows of data
              if (have_rows('resources') ): ?>
                <ul class="grid-desktop system-resources">
                <?php while( have_rows('resources') ) : the_row(); ?>
                  <li class="col-4-12-desktop system-resource">
                    <?php
                      $image = get_sub_field('image');
                      echo '<img class="system-resources-icon" src="' . $image['url'] . '" alt="' . $image['alt'] . '" />'; ?>
                      <h3 class="system-resources-header">
                        <?php the_sub_field('resource_header'); ?>
                      </h3>

                      <?php if (have_rows('calls_to_action') ): ?>
                        <ul class="system-resources-ctas">
                        <?php while( have_rows('calls_to_action') ) : the_row();
                          $call_to_action = get_sub_field('call_to_action');
                          $call_to_action_background_color = 'btn-' . get_sub_field('call_to_action_color');
                        ?>
                          <li class="system-resources-cta">
                            <a href="<?php echo $call_to_action['url']; ?>" class="btn <?php echo $call_to_action_background_color ?>" href="<?php echo $call_to_action['title']; ?>">
                              <?php echo $call_to_action['title']; ?>
                            </a>
                          </li>
                        <?php endwhile; ?>
                        </ul>
                      <?php endif; ?>
                  </li>
                <?php endwhile; ?>
                </ul>
              <?php endif; ?>


            </div>
          </div>
        <?php
          endwhile;
        endif; ?>

		<?php endwhile; ?>
    </div>
  </div>

<?php get_footer(); ?>
