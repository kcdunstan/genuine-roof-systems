<?php
/**
* Template Name: Why Replace
*
*/
?>

<?php get_header(); ?>


  <div class="main">
		<?php while ( have_posts() ) : the_post(); ?>

        <?php if ( has_post_thumbnail() ) : ?>
          <div class="hero">
            <?php the_post_thumbnail('full', array('class' => 'hero-image')); ?>
          </div>
        <?php endif; ?>


        <div class="why-replace-container why-replace-intro">
          <div class="page-header-container">
            <?php
              the_title('<h1 class="page-header">', '</h1>');
              echo '<h2 class="page-subheader">' . get_field('subheader') . '</h2>';
              $why_replace_image_1 = get_field('image_1');
              $why_replace_image_3 = get_field('image_3');
            ?>
          </div>
          <div class="grid-desktop">
            <div class="why-replace-text-block-1">
              <?php the_field('text_block_1'); ?>
            </div>
            <img src="<?php echo $why_replace_image_1['url']; ?>"
                class="why-replace-image why-replace-image-1" alt="<?php echo $why_replace_image_1['alt']; ?>" />
          </div>

          <h2 class="text-left page-subheader"><?php the_field('section_2_header'); ?></h2>
          <div class="why-replace-text-block-2">
            <?php the_field('text_block_2'); ?>
          </div>
        </div>

        <?php
  		  // check if the field has data

        if (get_field('video_header') ):

          $video_header = get_field('video_header');
          $video_file = get_field('video_file');
          $video_embedded_youtube_url = get_field('video_embedded_youtube_url');
          $video_caption = get_field('video_caption');
          $video_aspect_ratio = get_field('video_aspect_ratio');

        ?>
        <div class="wrapper wrapper-grey">
          <div class="video-container why-replace-container container">
            <h2 class="text-left"><?php echo $video_header; ?></h2>

            <?php if ($video_caption): ?>

              <?php echo $video_caption; ?>

            <?php endif; ?>

            <?php if ($video_file): ?>
              <div class="comparison-video embed-responsive embed-responsive-<?php echo $video_aspect_ratio; ?>">
                <video controls>
                  <source src="<?php echo $video_file['url']; ?>" type="video/mp4">
                </video>
              </div>

            <?php elseif ($video_embedded_youtube_url): ?>

              <div class="embed-responsive embed-responsive-<?php echo $video_aspect_ratio; ?>">
                <iframe class="embed-responsive-item" src="<?php echo $video_embedded_youtube_url; ?>" allowfullscreen></iframe>
              </div>

            <?php endif; ?>

          </div>

        </div>

        <?php endif; ?>

        <div class="wrapper">
          <div class="why-replace-container">
            <h2 class="text-left page-subheader"><?php the_field('section_3_header'); ?></h2>
            <div class="grid-desktop">
              <img src="<?php echo $why_replace_image_3['url']; ?>"
                  class="why-replace-image why-replace-image-3" alt="<?php echo $why_replace_image_3['alt']; ?>" />

              <div class="why-replace-text-block-3">
                <?php the_field('text_block_3'); ?>
              </div>
            </div>
          </div>
        </div>

		<?php endwhile; ?>
    </div>
  </div>

<?php get_footer(); ?>
