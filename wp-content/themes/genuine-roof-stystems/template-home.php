<?php
/**
* Template Name: Home
*
*/
?>

<?php get_header(); ?>


  <div class="main">
		<?php while ( have_posts() ) : the_post(); ?>
		  <?php
  		  // check if the repeater field has rows of data
        if (have_rows('carousel_slide') ): ?>

        <div class="carousel"
          data-flickity='{ "wrapAround": true }'>

        <?php // loop through the rows of data
            while ( have_rows('carousel_slide') ) : the_row();

              $background_image = get_sub_field('background_image');
              $background_position_desktop = get_sub_field('desktop_background_position');
              $background_position_mobile = get_sub_field('mobile_background_position');
              $overlay_position = get_sub_field('overlay_position');
              $orange_text = get_sub_field('orange_text');
              $headline = get_sub_field('headline');
              $body = get_sub_field('body');
              $product_color_image = get_sub_field('product_color_image');
              $call_to_action = get_sub_field('call_to_action');
              $display_product_logos = get_sub_field('display_product_logos');

              // display a sub field value
              ?>
              <div class="carousel-cell <?php echo $background_position_desktop . ' ' . $background_position_mobile?>" style="background-image: url('<?php echo $background_image; ?>');">

                <div class="carousel-overlay carousel-overlay-<?php echo $overlay_position; ?>">

                  <div class="carousel-overlay-inner">

                    <h2 class="carousel-header"><span class="carousel-subheader"><?php echo $orange_text; ?></span> <?php echo $headline; ?></h2>

                    <p><?php echo $body; ?></p>

                    <?php if ($product_color_image) : ?>

                      <img class="carousel-overlay-img" alt="<?php echo $product_color_image['alt']; ?>" src="<?php echo $product_color_image['url']; ?>" />

                    <?php endif; ?>

                    <?php if ($display_product_logos == 'true') : ?>

                    <div class="carousel-logos carousel-logos-mobile">
                      <img src="<?php echo get_stylesheet_directory_uri() ?>/img/logo-slatetec.svg" class="carousel-logo" />
                      <img src="<?php echo get_stylesheet_directory_uri() ?>/img/logo-tiletec.svg" class="carousel-logo" />
                      <img src="<?php echo get_stylesheet_directory_uri() ?>/img/logo-woodtec.svg" class="carousel-logo" />
                    </div>

                <?php endif; ?>

                    <?php echo '<a class="btn" href="'. $call_to_action['url'] .'" target="' . $call_to_action['target'] . '">' . $call_to_action['title'] . '</a>'; ?>

                  </div>

                </div>

                <?php if ($display_product_logos == 'true') : ?>

                    <div class="carousel-logos carousel-logos-desktop carousel-logos-<?php echo $overlay_position; ?>">
                      <img src="<?php echo get_stylesheet_directory_uri() ?>/img/logo-slatetec.svg" class="carousel-logo" />
                      <img src="<?php echo get_stylesheet_directory_uri() ?>/img/logo-tiletec.svg" class="carousel-logo" />
                      <img src="<?php echo get_stylesheet_directory_uri() ?>/img/logo-woodtec.svg" class="carousel-logo" />
                    </div>

                <?php endif; ?>

              </div>

              <?php

            endwhile;

        ?>

        </div>

        <?php

        else :

            // no rows found

        endif;


		    ?>

        <?php
  		  // check if the repeater field has rows of data
        if (have_rows('home_comparison') ): ?>

        <div class="wrapper">
          <div class="comparison-container" id="comparison-container">
            <div class="grid-desktop">
            <?php // loop through the rows of data
              while ( have_rows('home_comparison') ) : the_row();

                $home_comparison_item_logo = get_sub_field('home_comparison_item_logo');
                $home_comparison_item_illustration = get_sub_field('home_comparison_item_illustration');
                $home_comparison_item_header = removeWeirdCharacters(get_sub_field('home_comparison_item_header'));
                $home_comparison_item_subheader = removeWeirdCharacters(get_sub_field('home_comparison_item_subheader'));
                $home_comparison_video_url = get_sub_field('home_comparison_video_url');
                $home_comparison_video_button_text = get_sub_field('home_comparison_video_button_text');

                ?>

              <div class="comparison-col col-6-12-desktop">
                <img class="comparison-logo" alt="<?php echo $home_comparison_item_logo['alt']; ?>" src="<?php echo $home_comparison_item_logo['url']; ?>" />
                <img class="comparison-illustration"  alt="<?php echo $home_comparison_item_illustration['alt']; ?>" src="<?php echo $home_comparison_item_illustration['url']; ?>" />
                <h3 class="text-center comparison-weight text-color-orange"><?php echo $home_comparison_item_header; ?></h3>
                <p class="comparison-caption text-center"><?php echo $home_comparison_item_subheader; ?></p>
                <a href="#comparison-container" class="comparison-demo-link btn" data-video-url="<?php echo $home_comparison_video_url['url']; ?>"><?php echo $home_comparison_video_button_text; ?></a>
              </div>
          <?php

              endwhile;

          ?>
            </div>

            <div class="comparison-video-container">
              <button class="btn  btn-close comparison-video-close" data-item-detail-close aria-label="Close">&times;</button>
              <div class="comparison-video embed-responsive embed-responsive-840by330">
                <video controls>
                  <source src="" type="video/mp4">
                </video>
              </div>
            </div>

          </div>
        </div>
        <?php

        else :

            // no rows found

        endif;


		    ?>

      <?php
  		  // check if the repeater field has rows of data
        if (have_rows('genuine_roof_systems_overview') ):

          $genuine_roof_systems_overview_header = get_field('genuine_roof_systems_overview_header');

        ?>


        <div class="interlayment-system-wrapper wrapper wrapper-grey">
          <div class="interlayment-system-container container clearfix">
            <h2 class="interlayment-system-header"><?php echo $genuine_roof_systems_overview_header; ?></h2>

            <?php // loop through the rows of data
            while ( have_rows('genuine_roof_systems_overview') ) : the_row();

              $overview_image = get_sub_field('overview_image');
              $overview_image_header = get_sub_field('overview_image_header');
              $overview_image_subheader = get_sub_field('overview_image_subheader');
              $overview_call_to_action = get_sub_field('overview_call_to_action');
              $body_header = get_sub_field('body_header');
              $body = get_sub_field('body');
              $body_call_to_action = get_sub_field('body_call_to_action');

          ?>

          <div class="clearfix">
            <div class="interlayment-system-img-container">

              <img alt="<?php echo $overview_image['alt']; ?>" src="<?php echo $overview_image['url']; ?>" />

              <h4><strong class="block"><?php echo $overview_image_header; ?></strong> <?php echo $overview_image_subheader; ?></h4>

              <?php

              if ($overview_call_to_action) {

                echo '<p class="text-center"><a href="'. $overview_call_to_action['url'] .'" target="' . $overview_call_to_action['target'] . '" class="btn btn-centered">' . $overview_call_to_action['title'] . '</a></p>';

              }

              ?>

            </div>

            <div class="interlayment-system-text-container">
              <h3><?php echo $body_header; ?></h3>
              <?php
                echo $body;

                if ($body_call_to_action) {

                  echo '<p><a href="'. $call_to_action['url'] .'" target="' . $call_to_action['target'] . '" class="btn btn-text pull-right">' . $call_to_action['title'] . '</a></p>';

                }
              ?>

            </div>
          </div>

          <?php
            endwhile;
          ?>

        </div>
          <?php else :

              // no rows found

          endif;


  		    ?>

      <?php
		  // check if the repeater field has rows of data
      if (get_field('video_header') ):

        $video_header = get_field('video_header');
        $video_file = get_field('video_file');
        $video_embedded_youtube_url = get_field('video_embedded_youtube_url');
        $video_caption = get_field('video_caption');
        $video_aspect_ratio = get_field('video_aspect_ratio');

      ?>
        </div>
      </div>
      <div class="wrapper">
        <div class="video-container container">
          <h2 class="text-left"><?php echo $video_header; ?></h2>

          <?php if ($video_file): ?>
            <div class="comparison-video embed-responsive embed-responsive-<?php echo $video_aspect_ratio; ?>">
              <video controls>
                <source src="<?php echo $video_file['url']; ?>" type="video/mp4">
              </video>
            </div>

          <?php elseif ($video_embedded_youtube_url): ?>

            <div class="embed-responsive embed-responsive-<?php echo $video_aspect_ratio; ?>">
              <iframe class="embed-responsive-item" src="<?php echo $video_embedded_youtube_url; ?>" allowfullscreen></iframe>
            </div>

          <?php endif; ?>

          <?php if ($video_caption): ?>

            <div class="embed-responsive-caption">
              <?php echo $video_caption; ?>
            </div>

          <?php endif; ?>

        </div>

        <hr class="container" />

      </div>

      <?php endif; ?>

      <?php
        if (get_field('replacement_and_new_construction_projects_header')):
          $replacement_and_new_construction_projects_header = get_field('replacement_and_new_construction_projects_header');
          $replacement_and_new_construction_projects_column_1 = get_field('replacement_and_new_construction_projects_column_1');
          $replacement_and_new_construction_projects_column_2 = get_field('replacement_and_new_construction_projects_column_2');

      ?>
      <div class="wrapper">
        <div class="container">
          <h2 class="text-left"><?php echo $replacement_and_new_construction_projects_header; ?></h2>
          <div class="grid-desktop">
            <div class="col-7-12-desktop text-left">
              <?php echo $replacement_and_new_construction_projects_column_1; ?>
            </div>

            <div class="col-5-12-desktop">
              <?php echo $replacement_and_new_construction_projects_column_2; ?>
            </div>
          </div>
        </div>
      </div>

      <?php endif; ?>

		<?php endwhile; ?>
  </div>


<?php get_footer(); ?>
