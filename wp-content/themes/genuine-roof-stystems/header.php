<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7 ie6"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie10 lt-ie9 lt-ie8 ie7"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10 ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

	<head>

		<meta charset="UTF-8">

		<?php if (is_home ()) { ?>
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<?php } else { ?>
		<meta name="description" content="<?php $excerpt = strip_tags(get_the_excerpt()); echo $excerpt; ?>">
		<?php } ?>


		<meta name="twitter:card" content="summary">
		<meta name="twitter:site" content="<?php echo get_theme_mod( 'twitter_setting' ); ?>">
		<meta name="twitter:title" content="<?php bloginfo('name'); ?>">
		<meta name="twitter:description" content="<?php bloginfo('description'); ?>">

		<meta property="og:type" content="article">
		<meta property="og:title" content="<?php bloginfo('name'); ?>">
		<meta property="og:description" content="<?php bloginfo('description'); ?>">
		<meta property="og:site_name" content="<?php bloginfo('name'); ?>">

		<?php if ( has_post_thumbnail() ) { ?>
		<meta name="twitter:image" content="<?php $thumb_id = get_post_thumbnail_id(); $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true); echo $thumb_url[0]; ?>">
		<meta property="og:image" content="<?php echo $thumb_url[0]; ?>">

		<?php } else { ?>
		<meta name="twitter:image" content="<?php echo esc_url( get_theme_mod( 'nada_logo' ) ); ?>">
		<meta property="og:image" content="<?php echo esc_url( get_theme_mod( 'nada_logo' ) ); ?>">
		<?php } ?>

		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta http-equiv="x-ua-compatible" content="ie=edge">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo_rss('name'); ?>" href="<?php bloginfo_rss('atom_url') ?>">


		<?php wp_head(); ?>




	</head>




	<body <?php body_class(); ?>>

		<section class="header-container" id="top">
			<header>

        <button class="nav-mobile-trigger">
  			  <span class="nav-mobile-trigger-line"></span>
          <span class="nav-mobile-trigger-line"></span>
          <span class="nav-mobile-trigger-line"></span>
  			</button>

        <div class="nav-secondary-wrapper">
          <div class="nav-secondary-container">
      			<a class="nav-logo-container" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo( 'name' ); ?>">
              <img alt="Genuine Roof Systems icon" class="nav-logo-icon" src="<?php echo get_stylesheet_directory_uri() ?>/img/logo-header-icon.svg" />
        			<img alt="Genuine Roof Systems logo text" class="nav-logo-text" src="<?php echo get_stylesheet_directory_uri() ?>/img/logo-header-text.svg" />
        			<img alt="Genuine Roof Systems logo" class="nav-logo-mobile" src="<?php echo get_stylesheet_directory_uri() ?>/img/logo-header-mobile.svg" />
      			</a>

      			<?php wp_nav_menu( array( 'menu' => 'Secondary Nav', 'container_class' => 'nav-secondary-nav-container', 'container' => 'nav', 'depth' => 1 ) ); ?>

          </div>
        </div>

        <div class="nav-primary-wrapper">
          <div class="nav-primary-container">
            <h2 class="nav-primary-header">Our Products &amp; System</h2>
            <?php wp_nav_menu( array( 'menu' => 'Main Nav', 'container_id' => 'nav', 'container_class' => 'nav-primary-nav', 'container' => 'nav', 'depth' => 0 ) ); ?>
          </div>
        </div>

        <div class="nav-mobile-wrapper">
          <?php wp_nav_menu( array( 'menu' => 'Mobile Nav', 'container_id' => 'nav', 'menu_class' => 'menu-mobile-nav', 'container_class' => 'nav nav-mobile', 'container' => 'nav', 'depth' => 0 ) ); ?>
        </div>

			</header>
		</section>
		<a href="/about-us/#free-consultation" class="call-now" aria-label="Call Genuine Roof Systems now">
  		<span class="call-now-text">CALL</span>
      <img class="call-now-icon" src="<?php echo get_stylesheet_directory_uri() ?>/img/icon-phone.svg" />
  		<span class="call-now-text">NOW</span></a>
