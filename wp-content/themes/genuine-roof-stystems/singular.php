<?php get_header(); ?>
    <?php if ( has_post_thumbnail() ) : ?>
      <div class="hero">
        <?php echo the_post_thumbnail(); ?>
      </div>
    <?php endif; ?>

		<?php while ( have_posts() ) : the_post(); ?>
			<div class="main blog blog-detail-container">
				<article id="post-<?php the_ID(); ?>" <?php post_class('grid-desktop post-blog'); ?>>

          <div class="col-3-12-desktop">
            <div class="post-date">
        			<?php echo get_the_date('m / d'); ?>
        			<span class="post-year"><?php echo get_the_date('Y'); ?></span>
            </div>
          </div>

          <div class="col-6-12-desktop">
            <div class="page-header-container">
              <h1 class="page-header"><?php echo get_field('category_override'); ?></h1>
              <h2 class="page-subheader"><?php the_title(); ?></h2>
            </div>

  					<?php the_content(); ?>

          </div>

          <div class="blog-link blog-link-previous">
            <?php previous_post_link('%link', 'Previous Post'); ?>
          </div>

          <div class="blog-link blog-link-next">
            <?php next_post_link('%link', 'Next Post'); ?>
          </div>

					<?php

  				if (!empty($_SERVER['HTTP_REFERER'])) {
        		  $backbuttonurl = htmlspecialchars($_SERVER['HTTP_REFERER']);
        		  echo '<a class="btn btn-back" href=' . $backbuttonurl . ' onclick="if (document.referrer.indexOf(window.location.host) !== -1) { history.go(-1); return false; } else { window.location.href = ' . get_site_url() . '; }">Back to Blog</a>';
        	}

  				?>
				</article>
			</div>
		<?php endwhile; ?>



<?php get_footer(); ?>
