<?php
// NOTE: nada theme removes jQuery by default. Readding jQuery so Visual Form Builder functions
function re_add_jquery() {

	wp_enqueue_script('jquery');

}

add_action('wp_enqueue_scripts', 're_add_jquery');

function unhook_parent_style() {

  wp_dequeue_style( 'fonts-style' );
  //wp_dequeue_style( 'bh-storelocator-plugin-styles' );

}

add_action( 'wp_enqueue_scripts', 'unhook_parent_style', 20 );

function my_theme_enqueue_assets() {

  wp_enqueue_style( 'compiled-scss',
      get_stylesheet_directory_uri() . '/css/main.css',
      array()
  );

  wp_enqueue_script( 'header-js',
      get_stylesheet_directory_uri() . '/js/header-js.js' );

  wp_enqueue_script( 'footer-js',
      get_stylesheet_directory_uri() . '/js/footer-js.js',
      array(),
      wp_get_theme()->get('Version'),
      true );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_assets', 25 );

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

add_image_size( 'blog_overview', 446, 200, array( 'center', 'center' ) );

function removeWeirdCharacters($content) {
    $content = preg_replace('/\x03/', '', $content);
    return $content;
}

add_filter('the_content', 'removeWeirdCharacters');
add_filter('the_title', 'removeWeirdCharacters');

?>
