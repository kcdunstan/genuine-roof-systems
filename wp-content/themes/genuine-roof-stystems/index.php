<?php

get_header();

?>

  <?php if ( get_the_post_thumbnail(83) ) : ?>
    <div class="hero">
      <?php echo get_the_post_thumbnail(83); ?>
    </div>
  <?php endif; ?>

  <div class="main blog-container">
    <div class="page-header-container">
      <h1 class="page-header">Blog</h1>
      <h2 class="page-subheader">The Latest Updates from Genuine Roofing Systems</h2>
    </div>

    <?php while ( have_posts() ) : the_post(); ?>
		<article>

			<div class="grid-desktop post-blog">
  			<div class="col-5-12-desktop post-thumbnail-container">
    			<div class="grid-desktop">
      			<div class="col-10-12-desktop">
        			<div class="post-thumbnail">
        			  <?php the_post_thumbnail(); ?>
        			</div>
        			<div class="post-category"><?php echo get_field('category_override'); ?></div>
      			</div>
      			<div class="col-2-12-desktop">
              <div class="post-date">
          			<?php echo get_the_date('m / d'); ?>
          			<span class="post-year"><?php echo get_the_date('Y'); ?></span>
              </div>
      			</div>
    			</div>
        </div>

  			<div class="col-7-12-desktop">
    			<h2 class="post-title"><a href="<?php the_permalink(); ?>"><span class="post-subtitle"><?php echo get_field('category_override'); ?></span> <?php the_title(); ?></a></h2>
    			<?php the_excerpt(); ?>
          <a class="post-read-more" href="<?php the_permalink(); ?>"><?php esc_html_e('Read More', 'nada'); ?> &rsaquo;</a>
  			</div>
			</div>

		</article>

		<?php endwhile; ?>


		<nav id="pagi">
			<?php echo paginate_links(); ?>
		</nav>
  </div>


<?php get_footer(); ?>
