    <div class="wrapper wrapper-grey">
      <div class="container">
        <div class="grid-desktop">
          <div class="col-7-12-desktop">

            <?php
              echo removeWeirdCharacters(get_post_field('post_content', 221));
            ?>

          </div>
          <div class="col-5-12-desktop"><?php echo do_shortcode("[vfb id='1']"); ?></div>
        </div>
      </div>
    </div>

    <section class="footer-container">
			<footer class="footer">
				<a class="footer-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo( 'name' ); ?>">
         <img class="footer-logo-img" alt="Genuine Roof Systems logo" src="<?php echo get_stylesheet_directory_uri() ?>/img/logo-footer.svg" />
  			</a>

  	    <p class="footer-copyright">&copy;<?php echo date("Y"); ?> Genuine Roof Systems. All rights reserved.</p>

  	    <ul class="footer-social">
      			<li class="footer-social-item">
      			  <a class="footer-social-link" href="https://www.facebook.com/slatetec1" title="Genuine Roof Systems on Facebook">
        			  <img class="footer-social-icon" alt="Facebook" src="<?php echo get_stylesheet_directory_uri() ?>/img/social-icon-facebook.svg" />
      			  </a>
            </li>
      			<li class="footer-social-item">
      			  <a class="footer-social-link" href="https://www.youtube.com/channel/UCL3e8PizWg7y5PEmPKF-DLQ" title="Genuine Roof Systems on Youtube">
        			  <img class="footer-social-icon" alt="Youtube" src="<?php echo get_stylesheet_directory_uri() ?>/img/social-icon-youtube.svg" />
      			  </a>
            </li>
      			<li class="footer-social-item">
      			  <a class="footer-social-link" href="#" title="Genuine Roof Systems on Twitter">
        			  <img class="footer-social-icon" alt="Twitter" src="<?php echo get_stylesheet_directory_uri() ?>/img/social-icon-twitter.svg" />
      			  </a>

    			</ul>
			</footer>
		</section>

    <div class="nav-mask"></div>

		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', '<?php echo get_theme_mod( 'ga_setting' ); ?>', 'auto');
		  ga('send', 'pageview');

		</script>

		<?php wp_footer(); ?>

	</body>
</html>
