<?php




remove_action('wp_head', 'feed_links_extra', 3 );
remove_action('wp_head', 'feed_links', 2 );
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link', 10, 0 );
remove_action('wp_head', 'start_post_rel_link', 10, 0 );
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0 );
remove_action('wp_head', 'wp_generator');
remove_action('wp_head','jetpack_og_tags');
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
add_filter( 'use_default_gallery_style', '__return_false' );

if ( ! isset( $content_width ) ) $content_width = 1400;


register_nav_menu('primary', 'Primary');

function theme_slug_setup() {
   add_theme_support( 'title-tag' );
   add_theme_support('automatic-feed-links');
	 add_theme_support('post-thumbnails');
} add_action( 'after_setup_theme', 'theme_slug_setup' );



if ( ! function_exists( '_wp_render_title_tag' ) ) {
	function theme_slug_render_title() {
?>
<title><?php wp_title( '|', true, 'right' ); ?></title>
<?php
	}
	add_action( 'wp_head', 'theme_slug_render_title' );
}


update_option( 'large_size_w', 1400 );



function nada_html5shiv() { ?>
	<!--[if lt IE 9]>
	<script src=" <?php echo get_template_directory_uri(); ?>html5.js"></script>
	<![endif]-->
<?php }
add_action('wp_head', 'nada_html5shiv');



function disable_self_ping( &$links ) {
	foreach ( $links as $l => $link )
		if ( 0 === strpos( $link, home_url() ) )
		unset($links[$l]);
} add_action( 'pre_ping', 'disable_self_ping' );



function add_page_excerpts() { add_post_type_support('page', 'excerpt'); }
add_action('init', 'add_page_excerpts');




function theme_styles() {
	//wp_deregister_script('jquery');
	wp_register_style( 'fonts-style', '//fonts.googleapis.com/css?family=Roboto:300,700', array(), null, null );
  wp_register_style( 'main-style', get_template_directory_uri() . '/style.min.css', array(), null, null );

  wp_enqueue_style( 'fonts-style' );
  wp_enqueue_style( 'main-style' );
}
add_action('wp_enqueue_scripts', 'theme_styles');


function nada_signup_customizer($wp_customize){

	$wp_customize->add_section('ga_settings_section', array(
	  'title'          => 'Google Analytics Code'
	 ));

	$wp_customize->add_section('footer_settings_section', array(
	  'title'          => 'Footer settings'
	 ));



	$wp_customize->add_setting('ga_setting', array(
	 'default'     => 'UA-XXXXX-XX',
 	 'sanitize_callback' => 'esc_textarea',

	 ));
	$wp_customize->add_control('ga_setting', array(
	 'label'   => 'Google Analytics Code:',
	 'section' => 'ga_settings_section',
	 'type'    => 'text'
	));


	$wp_customize->add_setting( 'nada_logo', array(
		'sanitize_callback' => 'esc_url_raw'
	));
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'nada_logo', array(
		'label'    => __( 'Logo', 'nada' ),
		'section'  => 'title_tagline',
		'settings' => 'nada_logo',
	)));


	$wp_customize->add_setting('footer_setting', array(
	 'default'     => 'Add some text here.',
	 'sanitize_callback' => 'nada_sanitize_text',
	 'transport' => 'postMessage',
	 ));
	$wp_customize->add_control('footer_setting', array(
	 'label'   => 'Footer text:',
	 'section' => 'footer_settings_section',
	 'type'    => 'textarea'
	));


	$wp_customize->add_setting('twitter_setting', array(
	 'default'     => '@yourtwitter',
	 'sanitize_callback' => 'esc_textarea'
	));
	$wp_customize->add_control('twitter_setting', array(
	 'label'   => 'Twitter Name',
	 'section' => 'footer_settings_section',
	 'type'    => 'text'
	));

	$wp_customize->add_setting('formurl_setting', array(
	 'default'     => 'Add your MailChimp for form action URL here',
	 'sanitize_callback' => 'esc_url'
	 ));
	$wp_customize->add_control('formurl_setting', array(
	 'label'   => 'Form action URL:',
	 'section' => 'footer_settings_section',
	 'type'    => 'text'
	));

	$wp_customize->add_setting('showform_setting', array(
	 'default'     => 0
	 ));
	$wp_customize->add_control('showform_setting', array(
	 'label'   => 'Show signup form',
	 'section' => 'footer_settings_section',
	 'type'    => 'checkbox',
	));

	$wp_customize->add_setting('showtwitter_setting', array(
	 'default'     => 0
	 ));
	$wp_customize->add_control('showtwitter_setting', array(
	 'label'   => 'Show Twitter follow button',
	 'section' => 'footer_settings_section',
	 'type'    => 'checkbox',
	));


	function nada_sanitize_text( $input ) {
		return wp_kses_post( force_balance_tags( $input, array( 'strong' => array(), 'a' => array('href') ) ) );
	}


} add_action('customize_register', 'nada_signup_customizer');


require 'theme-update-checker.php';
$example_update_checker = new ThemeUpdateChecker(	'nada', 'https://nadatheme.com/info.json' );
?>
