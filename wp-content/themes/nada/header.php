<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7 ie6"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie10 lt-ie9 lt-ie8 ie7"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10 ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
	
	<head>

		<meta charset="UTF-8">
		
		<?php if (is_home ()) { ?>
		<meta name="description" content="<?php bloginfo('description'); ?>">	
		<?php } else { ?>
		<meta name="description" content="<?php $excerpt = strip_tags(get_the_excerpt()); echo $excerpt; ?>">
		<?php } ?>
		
		
		<meta name="twitter:card" content="summary">
		<meta name="twitter:site" content="<?php echo get_theme_mod( 'twitter_setting' ); ?>">
		<meta name="twitter:title" content="<?php bloginfo('name'); ?>">
		<meta name="twitter:description" content="<?php bloginfo('description'); ?>">

		<meta property="og:type" content="article">
		<meta property="og:title" content="<?php bloginfo('name'); ?>">
		<meta property="og:description" content="<?php bloginfo('description'); ?>">
		<meta property="og:site_name" content="<?php bloginfo('name'); ?>">

		<?php if ( has_post_thumbnail() ) { ?>
		<meta name="twitter:image" content="<?php $thumb_id = get_post_thumbnail_id(); $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true); echo $thumb_url[0]; ?>">
		<meta property="og:image" content="<?php echo $thumb_url[0]; ?>">
		
		<?php } else { ?>
		<meta name="twitter:image" content="<?php echo esc_url( get_theme_mod( 'nada_logo' ) ); ?>">
		<meta property="og:image" content="<?php echo esc_url( get_theme_mod( 'nada_logo' ) ); ?>">
		<?php } ?>
				
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo_rss('name'); ?>" href="<?php bloginfo_rss('atom_url') ?>">

		
		<?php wp_head(); ?>

		
		
		
	</head>
	
	
	
	
	<body <?php body_class(); ?>>

			<section>
				<header>
					<?php if ( get_theme_mod( 'nada_logo' ) ) : ?>
					<div class="logo">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo esc_url( get_theme_mod( 'nada_logo' ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a>
					</div>
					<?php else : ?>
					<div class="title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></div>
					<?php endif; ?>
		
		
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container_id' => 'nav', 'container_class' => 'nav-collapse', 'container' => 'nav', 'depth' => 1 ) ); ?>	

			</header>
		</section>

	
	
	
	
	