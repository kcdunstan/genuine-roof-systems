<?php get_header(); ?>


			
			<section>

				<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
					<h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<?php if ( has_post_thumbnail() ) { the_post_thumbnail('large'); } ?>
					<?php the_content(); ?>

				</article>

				<?php endwhile; ?>
					

				<nav id="pagi">
					<?php previous_posts_link('Previous'); ?>
					<?php next_posts_link('Next'); ?>
				</nav>
									
			</section>
		

<?php get_footer(); ?>