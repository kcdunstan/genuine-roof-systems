/* Requires */
var gulp = require('gulp'),
    // Utilities
    gutil = require('gulp-util'),
    constants = require('./gulp-tasks/constants');

// Tasks
require('./gulp-tasks/sass');
require('./gulp-tasks/scripts');
require('./gulp-tasks/sprites');

/**
 * Helper: changeNotification
 * Logs an event to the console.
 *
 * @param {String} fType - The file type that was changed.
 * @param {String} eType - The event that occured.
 * @param {String} msg - Short description of the actions that are being taken.
 */
function changeNotification(fType, eType, msg) {
  gutil.log(gutil.colors.cyan.bold(fType), 'was', gutil.colors.yellow.bold(eType) + '.', msg);
}


/**
 * Task: `watch`
 * Watches the HTML, Sass, and JS for changes. On a file change,
 * will run builds file-type dependently
 */
gulp.task('watch', function () {
  gutil.log('Waiting for changes.');

  // Set up our streams
  var jsWatch = gulp.watch([constants.paths.jsSourcePath + '**/*.js'], ['js:compile']),
      sassWatch = gulp.watch([  constants.paths.sassPath + '**/*.scss',
                                constants.paths.imgPath + 'sprite.png'], ['sass']),
      spriteWatch = gulp.watch([constants.paths.spritePath], ['sprite:build']);

  // js needs to be linted
  jsWatch.on('change', function (ev) {
    changeNotification('JS file', ev.type, 'Linting JS.');
  });

  // Sass needs to get built
  sassWatch.on('change', function (ev) {
    changeNotification('Sass file', ev.type, 'Running Sass compilation.');
  });

  // Compile new sprite file and css
  spriteWatch.on('change', function (ev) {
    changeNotification('Sprite file', ev.type, 'Running sprite compilation.');
  });

});
gulp.task('build', ['sass', 'js:lint', 'sprite:build']);
