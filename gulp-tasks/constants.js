// Building blocks
var root        = '',
    production_path = 'wp-content/themes/genuine-roof-stystems/',
    css_path = root + production_path + 'css/',
    js_path = root + production_path + 'js/',
    js_source_path = root + 'js/';

module.exports = {
    // Export all of our path constants
    paths: {
        // Core
        root: root,
        // Styles
        cssPath: css_path,
        sassPath: root + 'sass/',
        mainCss: root + 'sass/main.scss',
        cssBuild: css_path + 'main.css',
        cssSrcMaps: css_path + 'main.css.map',
        // JS
        jsSourcePath: js_source_path,
        babelifiedPath: js_source_path + 'babelified/',
        vendorHeaderJsPath: js_source_path + 'header-js/vendor/',
        vendorFooterJsPath: js_source_path + 'footer-js/vendor/',
        headerJsPath: js_source_path + 'header-js/',
        footerJsPath: js_source_path + 'footer-js/',
        compiledJSPath: js_path,
        // Imgs
        imgPath: production_path + 'img/',
        spritePath: root + 'sprites/**/*.png',
        spriteSassPath: root + 'sass/sprites/**/*'
    }
};
