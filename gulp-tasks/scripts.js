var gulp          = require('gulp'),
    constants     = require('./constants'),
    babel         = require('gulp-babel'),
    eslint        = require('gulp-eslint'),
    clean         = require('gulp-clean'),
    runSequence   = require('run-sequence'),
    concat        = require('gulp-concat');


module.exports = {

    compile: gulp.task('js:compile',  function (callback) {
      runSequence(  'js:lint', // lint
                    'js:babelify', // convert ES6 magic to ES5
                    'js:concatFooterJs', // combine vendor and app footer JS
                    'js:concatHeaderJs', // combine vendor and app header JS
                    'js:cleanBabelifiedJS', // cleanup unnecessary individually converted ES5 JS files
                  callback);
    }),

    lint: gulp.task('js:lint', function () {
        return gulp.src([
            constants.paths.jsSourcePath + '**/*.js',
            '!'+constants.paths.vendorHeaderJsPath + '**/*.js',
            '!'+constants.paths.vendorFooterJsPath + '**/*.js',
            '!'+constants.paths.jsSourcePath + 'babelified/**/*.js'
        ])
            .pipe(eslint({
              useEslintrc: true
            }))
            .pipe(eslint.format())
            .pipe(eslint.failAfterError());
    }),

    babelify: gulp.task('js:babelify', function(){
        return gulp.src([
                constants.paths.jsSourcePath + '**/*.js',
                '!'+constants.paths.vendorHeaderJsPath + '**/*.js',
                '!'+constants.paths.vendorFooterJsPath + '**/*.js'
            ])
            .pipe(babel())
            .pipe(gulp.dest(constants.paths.babelifiedPath));
    }),

    cleanBabelifiedJS: gulp.task('js:cleanBabelifiedJS', function(){
        return gulp.src(constants.paths.babelifiedPath, {read: false})
        .pipe(clean());
    }),

    concatHeaderJs: gulp.task('js:concatHeaderJs', function () {
        return gulp.src([constants.paths.vendorHeaderJsPath + '**/*.js', constants.paths.headerJsPath + '**/*.js'])
          .pipe(concat('header-js.js'))
          .pipe(gulp.dest(constants.paths.compiledJSPath));
    }),

    concatFooterJs: gulp.task('js:concatFooterJs', function () {
        return gulp.src([constants.paths.vendorFooterJsPath + '**/*.js', constants.paths.footerJsPath + '**/*.js'])
          .pipe(concat('footer-js.js'))
          .pipe(babel({presets: ['babili']})) // minify converted js
          .pipe(gulp.dest(constants.paths.compiledJSPath));
    })
};
