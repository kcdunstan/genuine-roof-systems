var gulp            = require('gulp'),
    gutil           = require('gulp-util'),
    constants       = require('./constants'),
    slang           = require('gulp-slang'),
    merge           = require('merge-stream'),
    spritesmith     = require('gulp.spritesmith'),
    imagemin        = require('gulp-imagemin'),
    buffer          = require('vinyl-buffer'),
    cssName         = 'sprite.scss',
    imgName         = 'sprite.png',
    imgPath         = '../img/';

module.exports = {

    buildSprite: gulp.task('sprite:build', function (cb) {
        var spriteData = gulp.src(constants.paths.spritePath).pipe(spritesmith({
            cssName: cssName,
            imgName: imgName,
            imgPath: imgPath + imgName
        }));

        // split up the css and img destinations
        var imgStream = spriteData.img
            .pipe(buffer())
            .pipe(imagemin())
            .pipe(gulp.dest(constants.paths.imgPath));

        var cssStream = spriteData.css
            .pipe(gulp.dest(constants.paths.sassPath));

        return merge(imgStream, cssStream);
    })
};
