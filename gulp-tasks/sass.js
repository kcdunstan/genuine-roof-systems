var gulp          = require('gulp'),
    constants     = require('./constants'),
    sass          = require('gulp-sass'),
    plumber       = require('gulp-plumber'),
    sasslint      = require('gulp-sass-lint'),
    sourcemaps    = require('gulp-sourcemaps'),
    autoprefixer  = require('gulp-autoprefixer');


module.exports = {
    /**
     * Task: `sass`
     * Runs the sass build and slings the results to AEM.
     */
    sass: gulp.task('sass', ['sass:buildAndLint']),

    /**
     * Task: `sass:build`
     * Compiles the sass and writes sourcemaps.
     */
    buildSass: gulp.task('sass:build', function (cb) {
        gulp.src(constants.paths.mainCss)
            .pipe(plumber()) // Prevents pipe breaking due to error (for watch task)
            .pipe(sourcemaps.init())
            .pipe(sass({
                outputStyle: 'compressed',
                omitSourceMapUrl: true, // This is hardcoded in the main.scss due to resource path issues
                includePaths: [constants.paths.sassPath, constants.paths.components]
            }).on('error', sass.logError))
            .pipe(autoprefixer({ browsers: [
                'last 2 versions',
                'ie >= 11',
                'Safari >= 9'
            ]}))
            .pipe(sourcemaps.write('./', {
                addComment: false
            }))
            .pipe(plumber.stop())
            .pipe(gulp.dest(constants.paths.cssPath))
            .on('end', cb);
    }),

    buildAndLintSass: gulp.task('sass:buildAndLint', ['sass:lint'], function (cb) {
        gulp.src(constants.paths.mainCss)
            .pipe(plumber()) // Prevents pipe breaking due to error (for watch task)
            .pipe(sourcemaps.init())
            .pipe(sass({
                outputStyle: 'compressed',
                omitSourceMapUrl: true, // This is hardcoded in the main.scss due to resource path issues
                includePaths: [constants.paths.sassPath]
            }).on('error', sass.logError))
            .pipe(autoprefixer({ browsers: [
                'last 2 versions',
                'ie >= 11',
                'Safari >= 9'
            ]}))
            .pipe(sourcemaps.write('./', {
                addComment: false
            }))
            .pipe(plumber.stop())
            .pipe(gulp.dest(constants.paths.cssPath))
            .on('end', cb);
    }),

    lintSass: gulp.task('sass:lint', function () {
        return gulp.src([
                constants.paths.mainCss,
                constants.paths.sassPath+'**/*.scss',
                constants.paths.components+'/**/*.scss',
                '!'+constants.paths.sassPath+'sprite.scss',
                '!'+constants.paths.sassPath+'_variables.scss',
                '!'+constants.paths.sassPath+'_typography.scss',
                '!'+constants.paths.sassPath+'normalize.scss',
                '!'+constants.paths.sassPath+'_mixins.scss',
                '!'+constants.paths.sassPath+'_flexbox.scss',
                '!'+constants.paths.sassPath+'vendor/**.*'
            ])
            .pipe(plumber())
            .pipe(sasslint({
                rules: {
                  'no-color-literals': 0,
                  'no-ids': 0,
                  'no-important': 0,
                  'mixins-before-declarations': 0,
                  'no-css-comments': 0,
                  'no-vendor-prefixes': 0,
                  'nesting-depth': 0
                }
              }
            ))
            .pipe(sasslint.format())
            .pipe(sasslint.failOnError())
            .pipe(plumber.stop());
    })
};
